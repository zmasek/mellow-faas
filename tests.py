import json
import unittest

from app import app


class BasicTests(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        self.client = app.test_client()
        self.assertEqual(app.debug, False)
        with open('test.json', 'r') as trello_board:
            self.trello_board = json.loads(trello_board.read())
        self.maxDiff = None

    def tearDown(self):
        pass

    def test_main_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 405)

    def test_correct_input(self):
        response = self.client.post('/', data={'text': json.dumps(self.trello_board)})
        json_data = response.get_json()
        expected = {
            'file': (
                'PG1hcCB2ZXJzaW9uPSIwLjkuMCI+PG5vZGUgVEVYVD0iVGVzdCBib2FyZCIgRk9MREVEPSJmYWxz\n'
                'ZSIgUE9TSVRJT049InJpZ2h0IiBJRD0iNWM3NjkxZmMxMWI0MGM4YWI3ZThjNDFmIiBYX0NPR0dM\n'
                'RV9QT1NYPSIwIiBYX0NPR0dMRV9QT1NZPSIwIj48ZWRnZSBDT0xPUj0iI2M3NjMwOCIvPjxmb250\n'
                'IE5BTUU9IkhlbHZldGljYSIgU0laRT0iNTAiLz48bm9kZSBURVhUPSJUZXN0IGxpc3QiIEZPTERF\n'
                'RD0iZmFsc2UiIFBPU0lUSU9OPSJyaWdodCIgSUQ9IjVjNzY5MjA2NmE4YTEyMjJlMDg1NTAyNiI+\n'
                'PGVkZ2UgQ09MT1I9IiNhNTZhNTMiLz48Zm9udCBOQU1FPSJIZWx2ZXRpY2EiIFNJWkU9IjMxIi8+\n'
                'PG5vZGUgVEVYVD0iVGVzdCBjYXJkIiBGT0xERUQ9ImZhbHNlIiBQT1NJVElPTj0icmlnaHQiIElE\n'
                'PSI1Yzc2OTIwYzc0YmUwYjQzZGEzOTkxN2IiPjxlZGdlIENPTE9SPSIjOWEwMTkyIi8+PGZvbnQg\n'
                'TkFNRT0iSGVsdmV0aWNhIiBTSVpFPSIxMyIvPjwvbm9kZT48L25vZGU+PC9ub2RlPjwvbWFwPg==\n'),
            'status': ''}
        self.assertEqual(json_data, expected)
        self.assertEqual(response.status_code, 200)

    def test_keyerror(self):
        response = self.client.post('/', data={'text': '{"wrong": "json"}'})
        json_data = response.get_json()
        self.assertEqual(json_data, {'file': '', 'status': "No key 'name' in JSON"})
        self.assertEqual(response.status_code, 400)

    def test_exception(self):
        response = self.client.post('/', data={'text': ''})
        json_data = response.get_json()
        self.assertEqual(json_data, {'file': '', 'status': 'Malformed JSON.'})
        self.assertEqual(response.status_code, 400)


if __name__ == "__main__":
    unittest.main()
