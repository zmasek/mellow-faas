======
Mellow
======

This repository holds the FaaS code for the Mellow service. It is deployed to AWS Lambda by Bitbucket Pipelines.

.. image:: https://coveralls.io/repos/bitbucket/zmasek/mellow-faas/badge.svg?branch=master
   :target: https://coveralls.io/bitbucket/zmasek/mellow-faas?branch=master
   :alt: Coverage Status

`Mellow <https://mellow.offsetlab.net/>`_ service takes Trello JSON export and converts it to a MM file for Coggle. The front-end is in `mellow-fe <https://bitbucket.org/zmasek/mellow-fe/>`_ repository that is served with Netlify.
