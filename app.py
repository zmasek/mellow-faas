import base64
import json
import os
import bugsnag

from bugsnag.flask import handle_exceptions
from flask import Flask, request, jsonify
from flask_cors import CORS
from mellow import mellow
from lxml import etree

bugsnag.configure(api_key=os.environ.get('BUGSNAG_KEY'), project_root='.', asynchronous=False)

app = Flask(__name__)
CORS(app)
handle_exceptions(app)


@app.route('/', methods=['POST'])
def home():
    """Convert a Trello JSON export to a Coggle readable file for diagrams.

    Submit a form on POST with a parameter called 'text' that has a JSON string for a value. Return
    JSON dict with 'file' and 'status' keys. The 'status' key holds a string of any errors that
    happened during processing of the input. The 'file' key holds a base64 formatted string that
    can be used to recreate the .mm file in the browser.
    """
    b64 = ''
    status = ''
    code = 400
    try:
        trello = json.loads(request.form['text'])
        xml = mellow(trello)
        xml_bytes = etree.tostring(xml)
        b64 = base64.encodebytes(xml_bytes).decode('utf-8')
        code = 200
    except KeyError as e:
        status = 'No key {} in JSON'.format(e)
    except Exception as e:
        bugsnag.notify(e)
        status = 'Malformed JSON.'

    return jsonify(file=b64, status=status), code
